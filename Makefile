DOCKER = docker
DOCKER-COMPOSE = docker-compose
APP = $(DOCKER-COMPOSE) exec -T php /entrypoint
TOOLS = $(DOCKER-COMPOSE) run --rm tools
SYMFONY = $(TOOLS) bin/console
COMPOSER = $(TOOLS) composer

##
## Project
## -------
##

.DEFAULT_GOAL := help

help: ## Default goal (display the help message)
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-20s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

.PHONY: help

install: ## Launch the project
install: build start vendor cc

build: ## Prepare the project
build: docker-compose.yml docker-compose.override.yml.dist
	- cp -n docker-compose.override.yml.dist docker-compose.override.yml
	$(DOCKER-COMPOSE) build

start: ## Allow to start the services
start:
	$(DOCKER-COMPOSE) up --no-recreate -d

stop: ## Stop the containers
stop:
	@$(DOCKER_COMPOSE) stop

rm: ## Remove the containers and their volumes
rm:
	@$(DOCKER_COMPOSE) kill || true
	@$(DOCKER_COMPOSE) down -v --remove-orphans || true
	@$(DOCKER_COMPOSE) rm --all --force || true

.PHONY: build start stop rm

##
## Tools
## -------
##

vendor: ## Install the project dependencies
vendor: composer.lock
	$(COMPOSER) install -n --prefer-dist

cc: ## Clear the cache (by default, the dev env is used)
cc: var/cache
	$(SYMFONY) cache:clear --env=$(or $(ENV), dev)
